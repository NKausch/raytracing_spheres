# Raytracing from Scratch

## Table of Contents
- [Raytracing from Scratch](#raytracing-from-scratch)
  - [Table of Contents](#table-of-contents)
  - [Author](#author)
  - [Preamble](#preamble)
  - [Result](#result)
  - [Tasks](#tasks)
    - [(Fundamental) Rendering loop](#fundamental-rendering-loop)
    - [(Fundamental) Camera](#fundamental-camera)
    - [(Fundamental) Objects: shape](#fundamental-objects-shape)
    - [(Fundamental) Enhancing camera and rendering loop](#fundamental-enhancing-camera-and-rendering-loop)
    - [(Fundamental) Object material: diffuse](#fundamental-object-material-diffuse)
    - [(Fundamental) Object material: specular](#fundamental-object-material-specular)
    - [(Optional) Object material: specular transmission](#optional-object-material-specular-transmission)
    - [(Optional) Lights](#optional-lights)
    - [(Optional) Positioning and orienting camera](#optional-positioning-and-orienting-camera)
    - [(Optional) Animation](#optional-animation)

## Author
Niklas Kausch

## Preamble

Even if I am satisfied with the outcome, I've accidentally overwritten my output-progress once by testing a function to render an animation :( ... Since I am not using any version control like git etc. i am not able to reproduce the images in the course of creating this renderer. The multiple images with the Aliasing and the recursive rays are being rendered afterwards (now) on the final program. They are in a seperate folder named Aliasing and Recursion.

## Result
<img src="final.png">

## Tasks


### (Fundamental) Rendering loop

The Framebuffer contains the color for every pixel. The stream outputs the final image and sves it to the disk. The color of each pixel is represented by a vector (Vec3f).
The Library "geometry.h" sets the templates for the vectors and the abilitity to make mathematical calculations.
```
void render() {
    const int width    = 1024;
    const int height   = 768;
    std::vector<Vec3f> framebuffer(width*height);

    for (size_t j = 0; j<height; j++) {
        for (size_t i = 0; i<width; i++) {
            framebuffer[i+j*width] = Vec3f(j/float(height),i/float(width), 0);
        }
    }

    std::ofstream ofs;
    ofs.open("out.ppm", std::ofstream::out | std::ofstream::binary);
    ofs << "P6\n" << width << " " << height << "\n255\n";
    for (size_t i = 0; i < height*width; ++i) {
        for (size_t j = 0; j<3; j++) {
            ofs << (char)(255 * std::max(0.f, std::min(1.f, framebuffer[i][j])));
        }
    }
    ofs.close();
}
```
The resulting image : 
<img src="loop.png" width="40%" height="40%">

### (Fundamental) Camera

The camera is not as an object implemented. The Idea behind this was the easy use of the origin for the camera. The implementation of a moving camera is described in "Positioning and orienting camera".

The following code snippet shows the final redering loop (shooting rays from origin/camera)
The implementation of the aliasing ist described in "enhancing camera and redering loop".

Basically the there is one ray shooted out of every pixel center (no AA) and being calculated by the cast_ray-function to archieve the coloring the pixel.
```
for (size_t j = 0; j<height; j++) {
        for (size_t i = 0; i<width; i++) {
            //Aliasing
            float ray_muls = 1./(antialiasing+1);
            Vec3f buffer;
            for(size_t x_rays = 1; x_rays<antialiasing+1; x_rays++) {
                for(size_t y_rays = 1; y_rays<antialiasing+1; y_rays++)  {
                    float x =  (2*(i + ray_muls*x_rays)/(float)width  - 1)*tan(fov/2.)*width/(float)height;
                    float y = -(2*(j + ray_muls*y_rays)/(float)height - 1)*tan(fov/2.);
                    Vec3f dir = Vec3f(x, y, -1).normalize();
                    buffer = buffer + cast_ray(Vec3f(0,0,0), dir, spheres, lights);
                }
            }
            framebuffer[i+j*width] = buffer/(antialiasing*antialiasing);
        }
    }
```

### (Fundamental) Objects: shape
Like mentioned in the task a sphere was used and can be configured by the parameters: 
1. Position by center
2. Size by radius
3. Appearence by material

Implemented by using a struct.
   
```
struct Sphere {
    Vec3f center;
    float radius;
    Material material;

    Sphere(const Vec3f &c, const float &r, const Material &m) : center(c), radius(r), material(m) {}
}
```

The spere has the abilitity to calculate a boolean to check if the ray hits the sphere with the ray_intersect-function. The color is not being calculated by the sphere itself. This is the task of the cast_ray-function from the "camera/environment".

In this the "real-object" of the spehre is being calculated
```
bool ray_intersect(const Vec3f &orig, const Vec3f &dir, float &t0) const {
    Vec3f L = center - orig;
    float tca = L*dir;
    float d2 = L*L - tca*tca;
    if (d2 > radius*radius) return false;
    float thc = sqrtf(radius*radius - d2);
    t0       = tca - thc;
    float t1 = tca + thc;
    if (t0 < 0) t0 = t1;
    if (t0 < 0) return false;
    return true;
}
```

### (Fundamental) Enhancing camera and rendering loop
Firtly I want to refer to the task:

1. For each pixel in image (rendering loop), generate multiple rays which are randomly positioned on pixel area.
2. Render images with 1, 2, 4, 8 and 16 rays per pixel.

I know the functionality of MSAA and understand the concept of placing them in the specific position like in this picture shown. [Link to image](https://i.stack.imgur.com/X4cqV.png)
<img src="msaa.png" width="80%" height="80%">

For the usage in this project a placement like this was not as practical, because the would be the need of a placement table or something similar. And the random placement didn't archieve good results in the qualitiy of the AA.

This was the reason a symmetrical position was being used. Cons of this is only the option of AA in x1, x4, x16, ... rays per pixel.
<img src="drawioAA.png" width="80%" height="80%">

To mention the implementation of the random position like in the task asked for:
Needing to create 2 random floats like -> 0 < random < 1 for the x and y offset of the ray inside the pixel.
```
        float x =  (2*(i + random_1)/(float)width  - 1)*tan(fov/2.)*width/(float)height;
        float y = -(2*(j + random_2)/(float)height - 1)*tan(fov/2.);
        Vec3f dir = Vec3f(x, y, -1).normalize();
        buffer = buffer + cast_ray(Vec3f(0,0,0), dir, spheres, lights);
.
.
.
framebuffer[i+j*width] = buffer/(amount_rays);
```

As you can see in the folder "Aliasing" there is a the higher the amount of rays ist, the smaller are the color differences at the edges.

### (Fundamental) Object material: diffuse
### (Fundamental) Object material: specular
### (Optional) Object material: specular transmission
These three tasks are archieved by tracing the ray and lettig it intersect with the different surfaces.
Main part of this is the amount of the color and the reflectance of the material.

Defining the materials and setting their parameters:
```
Material(const float &r, const Vec4f &a, const Vec3f &color, const float &spec) : refractive_index(r), albedo(a), diffuse_color(color), specular_exponent(spec) {}
Material() : refractive_index(1), albedo(1,0,0,0), diffuse_color(), specular_exponent() {}
float refractive_index;
Vec4f albedo;
Vec3f diffuse_color;
float specular_exponent;
```

The next steps are to calculate in which angle the rays intersect with the surfaces and how much of the material parameters correlate with the final raycolor. To show the bouncing a reflective scenario the following fuction makes it clear, how the ray is simply being turned, if it hits a fully reflective surface.
```
Vec3f reflect(const Vec3f &I, const Vec3f &N) {
    return I - N*2.f*(I*N);
}
```

All the different intensity types are being added together in the cast_ray-fuction to archive the final raycolor.
```
return material.diffuse_color * diffuse_light_intensity * material.albedo[0] + Vec3f(1., 1., 1.)*specular_light_intensity * material.albedo[1] + reflect_color*material.albedo[2] + refract_color*material.albedo[3];
```

The task demands a setting to adjust the recursion level for the "ray-bounces".
In the upper lines of the code you can find several settings.
```
const int width    = 1920;
const int height   = 1080;
const int fov      = M_PI/2.;

int reflection_recursion = 4;
int antialiasing = 1;
```

Unlike the Aliasing with multiple rays per pixel the recursion_depth has only a large visible difference until a depth of 4. 8 and 16  look similar in this test scene.

### (Optional) Lights

The Light implementation was relatively easy, because of the way the ray are being tracked through the cast_ray-function. This allows creating the light sources with only a few parameters and without complicated mathematical calculations like the sphres as struct. The calculations are already in the cast_ray-function.
```
struct Light {
    Light(const Vec3f &p, const float &i) : position(p), intensity(i) {}
    Vec3f position;
    float intensity;
};
```
To add a new light to the scene you just add it like a sphere. Parameters are only the position as vector and the intensity as float.
```
lights.push_back(Light(Vec3f(-20, 20000,  20), 4.));
```

### (Optional) Positioning and orienting camera

Like already mentioned, the Camera is being positioned in the origin. This allows
to archieve a positioning and orienting the camera by simply calculating the new positions of the spheres.

Concept:
1. Camera moves up
   1. Equals to moving all objects down
2. Camera turns right
   1. Equals to moving all objects to the left (attention, the distance has to be noted)

This allows also an implementation of "moving the camera". But it will always be a workaround.

### (Optional) Animation
This has not been implemented yet but the way i would do it is to render multiple single images with an offset of the objects and join the images together (maybe .gif or similar). For example the animation of a falling sphere. By calculating the position with the parameters of frames per second(fps) and the acceleration you can easily archieve a decent animation of a moving object.

Idea:   3s animation with 24 fps
```
last_pos = start_pos;
for (i=0;i<24*3;i++) {
    vertical_pos = last_pos - i^2;   //i^2 equals accelerating object
    place_sphere((X, vertical_pos, Z), ...);
    render();
}
```

This could be implemented in future, but for now I wish you happy easter ;)